use axum::{Extension, Json, Router, routing::get, http::StatusCode};
use serde::{Serialize, Deserialize};
use tracing::instrument;
use crate::hardware::pid::{Pid,PidHandle};

#[derive(Serialize,Deserialize)]
struct PidParameters {
  k_p: f64,
  k_i: f64,
  k_d: f64
}

#[derive(Serialize,Deserialize)]
struct SetpointParameters {
  setpoint: f64
}

#[derive(Serialize,Deserialize)]
struct CurrentPower {
  power: f64
}

#[instrument(skip(pid_handle))]
pub(super) fn router(pid_handle: PidHandle) -> Router {
  Router::new()
    .route("/parameters", get(get_parameters).post(set_parameters))
    .route("/setpoint", get(get_setpoint).post(set_setpoint))
    .route("/power", get(get_current_power))
    .route("/reset", get(reset_integral_term))
    .layer(Extension(pid_handle))
}

async fn get_parameters(Extension(pid): Extension<PidHandle>) -> Result<Json<PidParameters>, StatusCode> {
  let (k_p, k_i, k_d) = Pid::get_parameters(&pid).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))?;
  Ok(Json(PidParameters { k_p, k_i, k_d }))
}

async fn set_parameters(Extension(pid): Extension<PidHandle>, pid_parameters: Json<PidParameters>) -> Result<(), StatusCode> {
  let Json(PidParameters { k_p, k_i, k_d }) = pid_parameters;
  Pid::set_parameters(&pid, k_p, k_i, k_d).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))?;
  Ok(())
}

async fn get_setpoint(Extension(pid): Extension<PidHandle>) -> Result<Json<SetpointParameters>, StatusCode> {
  let setpoint = Pid::get_setpoint(&pid).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))?;
  Ok(Json(SetpointParameters { setpoint }))
}

async fn set_setpoint(Extension(pid): Extension<PidHandle>, setpoint_parameters: Json<SetpointParameters>) -> Result<(), StatusCode> {
  let Json(SetpointParameters { setpoint }) = setpoint_parameters;
  Pid::set_setpoint(&pid, setpoint).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))?;
  Ok(())
}

async fn get_current_power(Extension(pid): Extension<PidHandle>) -> Result<Json<CurrentPower>, StatusCode> {
  let last_output = Pid::last_output(&pid).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))?;
  Ok(Json(CurrentPower { power: last_output }))
}

async fn reset_integral_term(Extension(pid): Extension<PidHandle>) -> Result<(), StatusCode> {
  Pid::reset_integral_term(&pid).await.or(Err(StatusCode::INTERNAL_SERVER_ERROR))
}