use tracing::{instrument, info};
use axum::{
    Router,
};
use crate::hardware::pid::PidHandle;

mod pid;

#[instrument(skip(pid_handle))]
pub async fn start_axum(pid_handle: PidHandle) -> anyhow::Result<()> {
  let app = Router::new()
    .nest("/pid", pid::router(pid_handle))
    .layer(tower_http::trace::TraceLayer::new_for_http());

  let listener = tokio::net::TcpListener::bind("127.0.0.1:3000").await?;
  info!("Axum bound to http://{}", listener.local_addr()?);
  Ok(axum::serve(listener, app).await?)
}