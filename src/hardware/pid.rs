use anyhow::{anyhow, Result};
use tokio::sync::{mpsc, oneshot};
use tracing::{info, instrument};

pub type PidHandle = mpsc::Sender<PidMessage>;

#[derive(Debug)]
pub struct Pid {
  pid: pid::Pid<f64>,
  last_output: Option<f64>,
}

#[derive(Debug)]
pub enum PidMessage {
  GetParameters(Responder<(f64,f64,f64)>),
  SetParameters {
    p: f64,
    i: f64,
    d: f64,
  },
  GetSetpoint(Responder<f64>),
  SetSetpoint(f64),
  LastOutput(Responder<Option<f64>>),
  NextOutput {
    measurement: f64,
    resp: Responder<f64>,
  },
  ResetIntegralTerm,
}
type Responder<T> = oneshot::Sender<T>;

impl Pid {
  #[allow(clippy::new_ret_no_self)]
  #[allow(clippy::too_many_arguments)]
  pub fn new(setpoint: f64, output_limit: f64, p: f64, i: f64, d: f64, lim_p: f64, lim_i: f64, lim_d: f64) -> mpsc::Sender<PidMessage> {
    let mut pid = Pid { pid: pid::Pid::new(setpoint, output_limit), last_output: None };
    pid.pid.p(p, lim_p);
    pid.pid.i(i, lim_i);
    pid.pid.d(d, lim_d);

    let (tx, mut rx) = mpsc::channel(32);
    tokio::spawn(async move {
      while let Some(cmd) = rx.recv().await {
        pid.handle(cmd).await;
      }
      info!("PID shut down.");
    });

    tx
  }

  #[instrument]
  async fn handle(&mut self, cmd: PidMessage) {
    //debug!("doing pid");
    use PidMessage::*;
    match cmd {
      GetParameters(resp) => {
        let _ = resp.send((self.pid.kp, self.pid.ki, self.pid.kd));
      },
      SetParameters { p, i, d } => {
        self.pid.p(p, self.pid.p_limit);
        self.pid.i(i, self.pid.i_limit);
        self.pid.d(d, self.pid.d_limit);
      },
      GetSetpoint(resp) => {
        let _ = resp.send(self.pid.setpoint);
      },
      SetSetpoint(setpoint) => {
        self.pid.setpoint(setpoint);
      },
      LastOutput(resp) => {
        let _ = resp.send(self.last_output);
      },
      NextOutput { measurement, resp } => {
        let output = self.pid.next_control_output(measurement).output.max(0.0);
        self.last_output = Some(output);
        let _ = resp.send(output);
      },
      ResetIntegralTerm => {
        self.pid.reset_integral_term();
      },
    };
  }

  pub async fn get_parameters(sender: &mpsc::Sender<PidMessage>) -> Result<(f64, f64, f64)> {
    let (resp_tx, resp_rx) = oneshot::channel();
    sender.send(PidMessage::GetParameters(resp_tx)).await?;
    Ok(resp_rx.await?)
  }

  pub async fn set_parameters(sender: &mpsc::Sender<PidMessage>, p: f64, i: f64, d: f64) -> Result<()> {
    sender.send(PidMessage::SetParameters { p, i, d }).await?;
    Ok(())
  }

  pub async fn get_setpoint(sender: &mpsc::Sender<PidMessage>) -> Result<f64> {
    let (resp_tx, resp_rx) = oneshot::channel();
    sender.send(PidMessage::GetSetpoint(resp_tx)).await?;
    Ok(resp_rx.await?)
  }

  pub async fn set_setpoint(sender: &mpsc::Sender<PidMessage>, setpoint: f64) -> Result<()> {
    sender.send(PidMessage::SetSetpoint(setpoint)).await?;
    Ok(())
  }

  pub async fn last_output(sender: &mpsc::Sender<PidMessage>) -> Result<f64> {
    let (resp_tx, resp_rx) = oneshot::channel();
    sender.send(PidMessage::LastOutput(resp_tx)).await?;
    resp_rx.await?.ok_or(anyhow!("Not yet calculated an output"))
  }

  pub async fn next_control_output(sender: &mpsc::Sender<PidMessage>, measurement: f64) -> Result<f64> {
    let (resp_tx, resp_rx) = oneshot::channel();
    sender.send(PidMessage::NextOutput { measurement, resp: resp_tx }).await?;
    Ok(resp_rx.await?)
  }

  pub async fn reset_integral_term(sender: &mpsc::Sender<PidMessage>) -> Result<()> {
    sender.send(PidMessage::ResetIntegralTerm).await?;
    Ok(())
  }
}