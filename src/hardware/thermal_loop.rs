use super::pid::{Pid, PidHandle};
use anyhow::Result;
use std::time::Duration;
use tokio::time::sleep;
use tracing::{instrument,info,debug};


pub async fn start(pid: PidHandle) -> Result<()> {
  tokio::spawn(async move {
    thermal_loop(pid).await
  });
  Ok(())
}

// Mocked Thermal Loop for testing purposes
#[cfg(not(target_os = "espidf"))]
#[instrument(skip(pid))]
async fn thermal_loop(pid: PidHandle) {
  info!("Starting thermal loop...");
  let mut temp = 24.0;
  let mut heat: f64 = 0.0;
  loop {
    sleep(Duration::from_millis(1000)).await;
    temp += heat * (50.0 / temp);
    let control = Pid::next_control_output(&pid, temp).await.unwrap_or(0.5);
    heat = (control * 0.01 + heat - 0.003).clamp(-0.05, 0.2);
    debug!("New temperature: {}°C. Heat {}. Control {}", temp, heat, control);
  }
}

#[cfg(target_os = "espidf")]
#[instrument(skip(pid))]
async fn thermal_loop(pid: PidHandle) {
  info!("Starting thermal loop...");
  loop {
    sleep(Duration::from_millis(1000)).await;
    debug!("{:?}", Pid::get_setpoint(&pid).await.unwrap_or(-273.15));
    // get_temperature();
  }
}