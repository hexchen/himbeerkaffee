use anyhow::Result;


#[cfg(not(target_os = "espidf"))]
mod linux;
#[cfg(target_os = "espidf")]
mod esp32;

pub(crate) fn init() -> Result<()> {
  #[cfg(not(target_os = "espidf"))]
  use linux::*;
  #[cfg(target_os = "espidf")]
  use esp32::*;

  init_platform();
  init_logging();

  init_tokio()?;

  Ok(())
}

async fn async_main() -> Result<()> {
  let temperature_pid = crate::hardware::pid::Pid::new(94.0, 1.0, 0.07, 0.05, 0.9, 10.0, 1.0, 10.0);

  crate::hardware::thermal_loop::start(temperature_pid.clone()).await?;

  crate::http::start_axum(temperature_pid.clone()).await?;

  Ok(())
}