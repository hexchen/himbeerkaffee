use rtt_target::{ChannelMode::NoBlockSkip, UpChannel};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use esp_idf_hal::prelude::Peripherals;
use esp_idf_svc::eventloop::EspSystemEventLoop;
use esp_idf_svc::nvs::EspDefaultNvsPartition;
use esp_idf_svc::timer::EspTaskTimerService;
use esp_idf_svc::wifi::{AsyncWifi, EspWifi};
use esp_idf_sys::{esp, EspError};
use embedded_svc::wifi::{ClientConfiguration, Configuration};

use std::sync::{Mutex};
use tracing::info;
use tracing_subscriber::filter::LevelFilter;
use anyhow::Result;

pub(crate) fn init_platform() {
  esp_idf_svc::sys::link_patches();
}

struct UpChannelWriter {
  up_channel: UpChannel,
}
impl std::io::Write for UpChannelWriter {
  fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
    Ok(self.up_channel.write(buf))
  }

  fn flush(&mut self) -> std::io::Result<()> {
    Ok(())
  }
}

pub(super) fn init_logging() {
  let channels = rtt_target::rtt_init! {
    up: {
      0: { // channel number
          size: 1024, // buffer size in bytes
          mode: NoBlockSkip, // mode (optional, default: NoBlockSkip, see enum ChannelMode)
          name: "Terminal" // name (optional, default: no name)
      }
    }
  };

  #[cfg(debug)]
  let level_filter = LevelFilter::DEBUG;
  #[cfg(not(debug))]
  let level_filter = LevelFilter::INFO;

  tracing_subscriber::registry()
    .with(level_filter)
    .with(tracing_subscriber::fmt::layer().with_writer(Mutex::new(UpChannelWriter { up_channel: channels.up.0 } )))
    .init();
}

pub(super) fn init_tokio() -> Result<()> {
  info!("Setting up eventfd...");
  let config = esp_idf_sys::esp_vfs_eventfd_config_t { max_fds: 1 };
  esp! { unsafe { esp_idf_sys::esp_vfs_eventfd_register(&config) } }?;

  info!("Setting up board...");
  let peripherals = Peripherals::take().unwrap();
  let sysloop = EspSystemEventLoop::take()?;
  let timer = EspTaskTimerService::new()?;
  let nvs = EspDefaultNvsPartition::take()?;

  info!("Initializing Wi-Fi...");
  let wifi = AsyncWifi::wrap(
    EspWifi::new(peripherals.modem, sysloop.clone(), Some(nvs))?,
    sysloop,
    timer.clone())?;

  info!("Starting async run loop");
  tokio::runtime::Builder::new_current_thread()
      .enable_all()
      .build()?
      .block_on(async move {
        let mut wifi_loop = WifiLoop { wifi };
        wifi_loop.configure().await?;
        wifi_loop.initial_connect().await?;

        info!("Starting async_main");
        tokio::spawn(super::async_main());

        wifi_loop.stay_connected().await
      })?;

  Ok(())
}


struct WifiLoop<'a> {
  wifi: AsyncWifi<EspWifi<'a>>,
}

impl<'a> WifiLoop<'a> {
  pub async fn configure(&mut self) -> Result<(), EspError> {
    info!("Setting Wi-Fi credentials...");
    self.wifi.set_configuration(&Configuration::Client(ClientConfiguration {
      ssid: "jalol".into(),
      password: "passwort?".into(),
      ..Default::default()
    }))?;

    info!("Starting Wi-Fi driver...");
    self.wifi.start().await
  }

  pub async fn initial_connect(&mut self) -> Result<(), EspError> {
    self.do_connect_loop(true).await
  }

  pub async fn stay_connected(mut self) -> Result<(), EspError> {
    self.do_connect_loop(false).await
  }

  async fn do_connect_loop(
    &mut self,
    exit_after_first_connect: bool,
  ) -> Result<(), EspError> {
    let wifi = &mut self.wifi;
    info!("Entering main Wi-Fi run loop...");
    loop {
      // Wait for disconnect before trying to connect again.  This loop ensures
      // we stay connected and is commonly missing from trivial examples as it's
      // way too difficult to showcase the core logic of an example and have
      // a proper Wi-Fi event loop without a robust async runtime.  Fortunately, we can do it
      // now!
      wifi.wifi_wait(|| wifi.is_up(), None).await?;

      info!("Connecting to Wi-Fi...");
      wifi.connect().await?;

      info!("Waiting for association...");
      wifi.ip_wait_while(|| wifi.is_up().map(|s| !s), None).await?;

      if exit_after_first_connect {
        return Ok(());
      }
      
    }
  }
}