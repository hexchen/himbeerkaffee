use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use anyhow::Result;
use tracing::info;

pub(super) fn init_platform() {}

pub(super) fn init_logging() {
  tracing_subscriber::registry()
    .with(
      tracing_subscriber::EnvFilter::try_from_default_env()
        .unwrap_or_else(|_| "himbeerkaffee=debug,tower_http=debug".into()),
    )
    .with(tracing_subscriber::fmt::layer())
    .init();
}

pub(super) fn init_tokio() -> Result<()> {
  info!("Starting async run loop");
  tokio::runtime::Builder::new_current_thread()
      .enable_all()
      .build()?
      .block_on(async move {
        super::async_main().await
      })?;

  Ok(())
}