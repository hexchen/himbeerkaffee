use tracing::info;

mod http;
mod hardware;
mod platform;

#[cfg(target_os = "espidf")]
esp_idf_sys::esp_app_desc!();

fn main() {
  platform::init().expect("oopsies");

  info!("exiting...");
}
